from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project, Company
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, SearchForm
from tasks.models import Task


# Create your views here.
@login_required
def list_projects(request):
    filtered_form = SearchForm(request.GET)

    if filtered_form.is_valid() and filtered_form.cleaned_data["name"]:
        name = filtered_form.cleaned_data["name"]
        projects = Project.objects.filter(owner=request.user, company=name)

    else:
        projects = Project.objects.filter(owner=request.user)
    companies = Company.objects.all()

    context = {
        "project_list": projects,
        "form": SearchForm(),
        "filtered_form": filtered_form,
        "companies": companies,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


def company_search(request, id):
    company = Company.objects.filter(company_id=id)
    context = {
        "company": company,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_notes(request, id):
    tasks = get_object_or_404(Task, id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "projects/task_notes.html", context)
