from django.forms import ModelForm, ModelChoiceField
from projects.models import Project, Company


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "company",
            "owner",
        ]


class SearchForm(ModelForm):
    name = ModelChoiceField(queryset=Company.objects.all(), required=False)

    class Meta:
        model = Company
        fields = [
            "name",
        ]
