# Generated by Django 4.1.7 on 2023-03-14 15:50

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0002_company"),
    ]

    operations = [
        migrations.RenameField(
            model_name="company",
            old_name="company",
            new_name="project",
        ),
    ]
