from django.db import models
from tasks.models import Task
from django.contrib.auth.models import User


# Create your models here.
class Notes(models.Model):
    name = models.CharField(max_length=200)
    detail = models.TextField()
    tasks = models.ForeignKey(
        Task,
        related_name="notes",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        null=True,
        related_name="notes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
