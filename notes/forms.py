from django.forms import ModelForm
from notes.models import Notes


class NoteForm(ModelForm):
    class Meta:
        model = Notes
        fields = [
            "name",
            "detail",
            "tasks",
            "assignee",
        ]
